CREATE TABLE IF NOT EXISTS {schema}.{file_table}(
	PRIMARY KEY (id),
	FOREIGN KEY (entity) REFERENCES public.entities(name),
	FOREIGN KEY (category) REFERENCES public.file_categories(name)
) INHERITS (public.{file_table});
