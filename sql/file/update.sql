UPDATE {schema}.{table}
SET 
	name = :name,
	file_size = :file_size,
	mime_type = :mime_type,
	entity = :entity,
	entity_id = :entity_id,
	link = :link,
	category = :category,
	updated_by = :updated_by,
	updated_at = now()

WHERE id = :id AND deleted = false
RETURNING *
