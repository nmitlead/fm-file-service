UPDATE {schema}.{table} 
SET 
	name=:name,
	description=:description, 
	vehicle_id=:vehicle_id, 
	odometer_id=:odometer_id,
	work_id=:work_id,
	due_date=:due_date,
	entry_date=:entry_date,
	due_odometer_value=:due_odometer_value,
	assigner=:assigner,
	status=:status,
	updated_by=:updated_by
WHERE id=:id AND deleted=false
