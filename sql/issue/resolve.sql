UPDATE {schema}.{table}
SET
	work_id=:work_id,
	status={resolved}
WHERE id IN ({issue_ids}) AND deleted=false
