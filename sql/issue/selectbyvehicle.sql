SELECT
    s.*,

    -- Odometer Columns
    od.id "od.id",
    od.vehicle_id "od.vehicle_id",
    od.value "od.value",
    od.is_void "od.is_void",

    od.created_at "od.created_at",
    od.updated_at "od.updated_at",
    od.deleted_at "od.deleted_at",
    od.created_by "od.created_by",
    od.updated_by "od.updated_by",
    od.deleted_by "od.deleted_by",
    od.deleted "od.deleted"

FROM {schema}.{table} s
INNER JOIN {schema}.{odometer_table} od ON s.odometer_id=od.id
WHERE s.deleted=false AND s.vehicle_id=$1
ORDER BY $2
LIMIT $3
OFFSET $4
