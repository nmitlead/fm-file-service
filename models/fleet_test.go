package models

import (
	"testing"
)

const (
	FLEET_ID = "ec4efaa0-620f-4632-bdec-5a25560b0d88"
)

func TestCreate(t *testing.T) {
	fleet_schema := "fleet_ec4efaa0_620f_4632_bdec_5a25560b0d88"

	if _, err := DB.Exec("CREATE SCHEMA IF NOT EXISTS " + fleet_schema + " ;"); err != nil {
		t.Error("schema create error", err)
	}

	m := &Fleet{}
	if err := m.Create(FLEET_ID); err != nil {
		t.Error("Fleet create error: ", err)
	}

	if _, err := DB.Exec("DROP SCHEMA " + fleet_schema + " CASCADE;"); err != nil {
		t.Error("schema create error", err)
	}

}
