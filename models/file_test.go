package models

import (
	"github.com/pborman/uuid"

	"io/ioutil"
	"os"
	"testing"
)

var (
	EntityId = "8b56e593-adb3-4d49-9dda-74995c37e10c"
	FileId   string
	UUID     = "e7ed944a-8fea-4272-9e9e-84c09a60dd8f"
)

func TestFileCreate(t *testing.T) {
	fileToBeUploaded := "test_data/insert.test"
	file, err := os.Open(fileToBeUploaded)
	if err != nil {
		t.Error(err)
	}

	file_bytes, err := ioutil.ReadAll(file)
	if err != nil {
		t.Error(err)
	}

	model := File{}
	model.Name = "insert.test"
	model.Size = 105
	model.Type = "text/test"
	model.Entity = "work"
	model.Category = "doc"
	model.EntityId = EntityId
	model.CreatedBy = UUID

	if err := model.Create(schema, file_bytes); err != nil {
		t.Error(err)
	}

	if model.Id == "" {
		t.Error("insert id not generated")
	}

	FileId = model.Id

	path := cf.UploadDir + schema + "/" + FileId + ".test"
	_, err = os.Open(path)
	if err != nil {
		t.Error("File not exists with this name: ", err, path)
	}
}

func TestFileGet(t *testing.T) {
	model := File{}
	if err := model.Get(schema, FileId); err != nil {
		t.Errorf("model get error %v", err)
		return
	}
}

func TestFileGetAll(t *testing.T) {
	model := File{}
	dests, pages, err := model.GetAll(schema, []string{""}, 0, 0)
	if err != nil {
		t.Error(err)
	}
	if len(dests) < 1 {
		t.Error("dests length should be grater than 0")
	}

	if pages < 1 {
		t.Error("pages should be grater than 0")
	}
}

func TestFileDelete(t *testing.T) {
	model := File{}
	model.Id = FileId
	model.DeletedBy.String = UID
	model.DeletedBy.Valid = true

	if err := model.Delete(schema); err != nil {
		t.Error(err)
	}
}

func TestFileRestore(t *testing.T) {
	model := File{}
	model.Id = FileId
	if err := model.Restore(schema); err != nil {
		t.Error(err)
	}
}

func TestFileCountByEntityCategory(t *testing.T) {
	model := File{}
	cnt, err := model.CountByEntityCategory(schema, "work", "doc", EntityId)
	if err != nil {
		t.Error(err)
	}
	if cnt < 1 {
		t.Error("count should be grater than 0")
	}
}

func TestFileGetByEntityCategory(t *testing.T) {
	model := File{}
	dests, pages, err := model.GetByEntityCategory(schema, "work", "doc", EntityId, []string{""}, 0, 0)
	if err != nil {
		t.Error(err)
	}
	if len(dests) < 1 {
		t.Error("dests length should be grater than 0")
		return
	}

	if pages < 1 {
		t.Error("pages should be grater than 0")
		return
	}
}

func TestFileUpdate(t *testing.T) {
	fileToBeUploaded := "test_data/update.test2"
	file, err := os.Open(fileToBeUploaded)
	if err != nil {
		t.Error(err)
	}

	file_bytes, err := ioutil.ReadAll(file)
	if err != nil {
		t.Error(err)
	}

	model := File{}
	model.Id = FileId
	model.Name = "update.test2"
	model.Size = 105
	model.Type = "text/test"
	model.Entity = "work"
	model.Category = "doc"
	model.EntityId = EntityId
	model.UpdatedBy.String = UUID
	model.UpdatedBy.Valid = true

	if err := model.Update(schema, file_bytes); err != nil {
		t.Error(err)
	}
}

func TestFileCreateInFileSystem(t *testing.T) {
	secondFileId := uuid.New()
	model := File{Id: secondFileId, Name: "create_in_fs.test"}

	fileToBeUploaded := "test_data/create_in_fs.test"
	file, err := os.Open(fileToBeUploaded)
	if err != nil {
		t.Error(err)
		return
	}

	file_bytes, err := ioutil.ReadAll(file)
	if err != nil {
		t.Error(err)
		return
	}

	if err := model.createInFileSystem(schema, file_bytes); err != nil {
		t.Error("create in file system error: ", err)
		return
	}
}

func TestFileGetByEntityCategoryEmpty(t *testing.T) {
	model := File{}
	dests, _, err := model.GetByEntityCategory(schema, "vehicle", "image", EntityId, []string{""}, 0, 0)
	if err == nil {
		t.Error("should be error")
	}

	if len(dests) != 0 {
		t.Errorf("dests length should be 0 got %d", len(dests))
		return
	}
}
