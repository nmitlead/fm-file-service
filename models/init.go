package models

import (
	"github.com/jmoiron/sqlx"

	"file/conf"
	"file/db"
	"fm-libs/orm"

	"errors"
	"fmt"
)

var (
	ORM *orm.Orm
	cf  *conf.AppConf
	DB  *sqlx.DB

	ErrNotFound     = errors.New("Not found in db")
	ErrPageNotFound = errors.New("Page not found")
)

func initConf() error {
	//for not creating config in every call
	if cf != nil {
		return nil
	}

	var err error
	cf, err = conf.GetConf()

	if err != nil {
		fmt.Errorf("get config error: ", err)
	}

	return nil
}

func Init() error {
	var err error

	if err := initConf(); err != nil {
		return err
	}

	DB, err = db.GetDb()
	if err != nil {
		return err
	}

	ORM, err = orm.New(DB, cf.SqlDir)
	if err != nil {
		return fmt.Errorf("orm initialize errro %s", err)
	}

	return nil
}
