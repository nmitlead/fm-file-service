package models

import (
	"github.com/pborman/uuid"

	"fm-libs/dbutil"
	"fm-libs/orm"
	"fm-libs/util"

	"bytes"
	"fmt"
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"
)

var MapCategoryToString = map[string]string{
	"image":   "image",
	"doc":     "doc",
	"profile": "profile",
}

var MapEntityToString = map[string]string{
	"vehicle": "vehicle",
	"work":    "work",
	"issue":   "issue",
	"user":    "user",
	"fleet":   "fleet",
}

type File struct {
	Id       string `db:"id" json:"id"`
	Name     string `db:"name" json:"name"`
	Size     int64  `db:"file_size" json:"size"`
	Type     string `db:"mime_type" json:"type"`
	Entity   string `db:"entity" json:"entity"`
	EntityId string `db:"entity_id" json:"entity_id"`
	Link     string `db:"link" json:"link"`
	Category string `db:"category" json:"category"`

	orm.Std
}

func (m File) GetName() string {
	return "file"
}

func (m File) GetTable() string {
	return "file_files"
}

func (m *File) SetId(id string) {
	m.Id = id
}

// Checks if given entity is correct by checking if it is in the map.
// Then sets it.
func (m *File) SetEntity(entity string) error {
	entity_string, ok := MapEntityToString[entity]

	if !ok {
		return fmt.Errorf("invalid entity: %s", entity)
	}
	m.Entity = entity_string
	return nil
}

// Checks if given category is correct by checking if it is in the map.
// Then sets it.
func (m *File) SetCategory(category string) error {
	category_string, ok := MapCategoryToString[category]

	if !ok {
		return fmt.Errorf("invalid category: %s", category)
	}
	m.Category = category_string
	return nil
}

func (m *File) createInFileSystem(schema string, file []byte) error {
	if err := os.MkdirAll(cf.UploadDir+schema, os.ModePerm+os.ModeDir); err != nil {
		return err
	}

	createdFile, err := os.Create(cf.UploadDir + schema + "/" + m.Id + path.Ext(m.Name))
	if err != nil {
		return err
	}

	defer createdFile.Close()

	_, err = io.Copy(createdFile, bytes.NewReader(file))
	return err
}

// Implements WalkerFunc for filepath.Walk function
// Delete all files in directory starting with id of file
func (m *File) deleteFiles(path string, f os.FileInfo, err error) (e error) {
	// check each file if starts with the word "dumb_"
	if strings.HasPrefix(f.Name(), m.Id) {
		os.Remove(path)
	}

	return
}

// Creates a database entry and then uploads the given file
func (m *File) Create(schema string, file []byte) error {
	m.Id = uuid.New()
	m.Link = cf.BaseUrl + "/" + schema + "/" + m.Id + path.Ext(m.Name)

	if err := m.createInFileSystem(schema, file); err != nil {
		return err
	}

	return ORM.InsertWithoutId(m, orm.Args{"schema": schema, "table": m.GetTable()})
}

func (m *File) Update(schema string, file []byte) error {
	m.Link = cf.BaseUrl + "/" + schema + "/" + m.Id + path.Ext(m.Name)

	if err := filepath.Walk(cf.UploadDir+schema+"/", m.deleteFiles); err != nil {
		return err
	}

	if err := m.createInFileSystem(schema, file); err != nil {
		return err
	}

	return ORM.Update(m, orm.Args{"schema": schema, "table": m.GetTable()})
}

func (m *File) Get(schema, id string) error {
	args := orm.Args{
		"table":  m.GetTable(),
		"schema": schema,
	}
	return ORM.GetByPk(m, args, id)
}

func (m *File) Count(schema string) (int, error) {
	var count int
	args := orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}
	err := ORM.Get(&count, orm.ModelSqlFile(m, "count"), args)

	if err != nil {
		return 0, err
	}

	return count, nil
}

func (m *File) GetAll(schema string, sort []string, page, pageSize int) ([]File, int, error) {
	dests := []File{}

	order := dbutil.OrderMap(map[string]string{
		"id":   "id",
		"name": "name",
	}, sort, "id ASC")

	cnt, err := m.Count(schema)
	if err != nil {
		return nil, 0, err
	}

	offset, pages := util.GetPageOffset(page, pageSize, cnt)

	if offset == -1 {
		return nil, 0, ErrNotFound
	}

	if pages == -1 {
		return nil, 0, ErrPageNotFound
	}

	if pageSize < 1 {
		pageSize = 10
	}

	err = ORM.Select(&dests, orm.ModelSqlFile(m, "select"), orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}, order, pageSize, offset)

	if err != nil {
		return nil, 0, err
	}

	return dests, pages, nil
}

func (m *File) Delete(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Delete(m, args)
}

func (m *File) Restore(schema string) error {
	args := orm.Args{"schema": schema, "table": m.GetTable()}
	return ORM.Restore(m, args)
}

func (m *File) CountByEntityCategory(schema, entity, category, entity_id string) (int, error) {
	var count int
	args := orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}
	err := ORM.Get(&count, orm.ModelSqlFile(m, "countbyentitycategory"), args, entity, category, entity_id)

	if err != nil {
		return 0, err
	}

	return count, nil
}

func (m *File) GetByEntityCategory(schema, entity, category, entity_id string, sort []string, page, pageSize int) ([]File, int, error) {
	dests := []File{}

	order := dbutil.OrderMap(map[string]string{
		"id":   "id",
		"name": "name",
	}, sort, "id ASC")

	cnt, err := m.CountByEntityCategory(schema, entity, category, entity_id)
	if err != nil {
		return nil, 0, err
	}

	offset, pages := util.GetPageOffset(page, pageSize, cnt)

	if offset == -1 {
		return nil, 0, ErrNotFound
	}

	if pages == -1 {
		return nil, 0, ErrPageNotFound
	}

	if pageSize < 1 {
		pageSize = 10
	}

	err = ORM.Select(&dests, orm.ModelSqlFile(m, "selectbyentitycategory"), orm.Args{
		"schema": schema,
		"table":  m.GetTable(),
	}, entity, category, entity_id, order, pageSize, offset)

	if err != nil {
		return nil, 0, err
	}

	return dests, pages, nil
}
