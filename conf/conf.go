package conf

import "fm-libs/config"

var (
	ConfDir = "conf"
	RunMode = "dev"

	appConf *AppConf
)

const ServiceName = "file"

type AppConf struct {
	Db    config.Db
	Redis config.Redis
	Rmq   config.Rmq
	Log   config.Log

	Addr       string
	SqlDir     string
	MigrateDir string

	MigrateTable string

	RoutingKeys struct {
		FleetCreate string
	}

	// Application version
	Version string
	// Dependencies
	Deps map[string]string

	UploadDir string
	BaseUrl   string
}

func (c *AppConf) SetDefaults() {
	c.Db.SetDefaults()
	c.Redis.SetDefaults()
	c.Rmq.Url = "amqp://localhost:5672"

	c.Addr = ":4000"
	c.SqlDir = "sql"
	c.MigrateDir = "migrations"

	c.MigrateTable = "file_migrations"

	c.RoutingKeys.FleetCreate = "fleet.fleet.created"

	c.Log.Lvl = "info"
	c.Log.ToRabbit = true
	c.Log.Dir = ""

	c.UploadDir = "static/"
	c.BaseUrl = "/files"

	c.Version = "0.2.0"
	c.Deps = map[string]string{
		"fm-libs": "0.4.0",
		"dic":     "0.2.0",
	}
}

func GetConf() (*AppConf, error) {
	var err error

	//appConf not initialized initialize it
	if appConf == nil {
		cf := AppConf{}

		cf.SetDefaults()

		confFile := ConfDir + "/app." + RunMode + ".toml"
		if err = config.ReadResources(&cf, confFile, "env"); err != nil {
			return nil, err
		}

		appConf = &cf
	}

	return appConf, nil
}
