package main

import (
	"github.com/jmoiron/sqlx"

	"file/conf"
	DB "file/db"
	"file/event"
	"file/handlers"
	"file/logger"
	"file/models"
	"fm-libs/dbutil"
	"fm-libs/log"

	"fmt"
	"github.com/fatih/color"
)

var (
	db *sqlx.DB
	cf *conf.AppConf //config
)

func Init() error {
	var err error

	err = logger.Init()
	if err != nil {
		return fmt.Errorf("init log error: %s", err)
	}

	cf, err = conf.GetConf()
	if err != nil {
		return fmt.Errorf("get conf error: %s", err)
	}

	db, err = DB.GetDb()
	if err != nil {
		return fmt.Errorf("get db error: %s", err)
	}

	//Initializing all packages
	if err := models.Init(); err != nil {
		return fmt.Errorf("models Init error: %s", err)
	}

	if err := handlers.Init(); err != nil {
		return fmt.Errorf("handlers Init error: %s", err)
	}

	if err := event.Init(); err != nil {
		return fmt.Errorf("event Init error: %s", err)
	}

	return nil
}

func PrintVersion() {
	color.Blue("Version: %s", cf.Version)
	color.Green("Dependencies:")
	for key, value := range cf.Deps {
		color.Magenta("\t%s: %s\n", key, value)
	}
}

func main() {

	if err := Init(); err != nil {
		log.Log("main init", log.ErrorLevel, log.M{"error": err.Error()})
	}

	// Printing version of app and it's dependencies
	PrintVersion()

	//running migrations
	n, err := dbutil.MigrateUp(db.DB, "postgres", cf.MigrateTable)
	if err != nil {
		log.Log("migrate up", log.FatalLevel, log.M{"error": err.Error(), "table": cf.MigrateTable})
	}

	log.Log("applied migrations", log.InfoLevel, log.M{
		"migrations": n,
	})

	//	listen fleet create event
	succ_chan, err_chan := event.ListenFleetCreate()

	go func() {
		for err := range err_chan {
			log.Log("event error", log.ErrorLevel, log.M{
				"error": err.Error(),
			})
		}
	}()

	go func() {
		for msg := range succ_chan {
			log.Log("event success", log.InfoLevel, log.M{
				"fleet": msg,
			})

		}
	}()

	log.Log("file started", log.InfoLevel, log.M{
		"at": cf.Addr,
	})

	handlers.Listen()
}
