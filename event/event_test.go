package event

import (
	"file/rabbit"
	"fm-libs/preputils"
	"testing"
	"time"
)

func TestListenFleet(t *testing.T) {
	succChan, errChan := ListenFleetCreate()

	if err := rabbit.Emit(
		cf.RoutingKeys.FleetCreate,
		preputils.FleetEventData{
			Data: preputils.Fleet{
				Id:        FID,
				CreatedBy: UUID,
			},
		},
	); err != nil {
		t.Error("publish error ", err)
	}

	select {
	case err := <-errChan:
		if err != nil {
			t.Error("Listen error ", err)
		}
	case <-succChan:
	case <-time.After(5 * time.Second):
		t.Error("Event not emitted")
	}

}
