-- +migrate Up

CREATE TABLE file_categories(
	name TEXT PRIMARY KEY
);

INSERT INTO file_categories(name) VALUES
('image'),
('profile'),
('doc');

ALTER TABLE file_files
ADD FOREIGN KEY(entity) REFERENCES entities(name),
ADD FOREIGN KEY(category) REFERENCES file_categories(name);

-- +migrate Down

ALTER TABLE file_files
DROP CONSTRAINT IF EXISTS file_files_entity_fkey,
DROP CONSTRAINT IF EXISTS file_files_category_fkey;

DROP TABLE IF EXISTS file_categories CASCADE;
